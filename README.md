# Data migration to OCI using a hard disk drive


## Introduction

Oracle offers offline data transfer solutions that let you migrate data to Oracle Cloud Infrastructure using either a user-supplied storage device, or an Oracle-supplied transfer appliance. Here we focus on user-supplied transfer storage device method.


## Disk-Based Data Import

Disk-Based Data Import is one of Oracle's offline data transfer solutions that lets you migrate data to OCI. In general, an import disk is a user-supplied storage device that is specially prepared to copy and upload data to OCI. You copy your data to the import disk and ship it to Oracle to upload your data where shipment means sending a parcel via the vendors available for delivering the transfer disk to OCI (DHL, UPS, FedEx). 

You can find more details [here](https://docs.oracle.com/en-us/iaas/Content/DataTransfer/Concepts/disk_overview.htm), please take into the consideration the following:
-	Supports external USB 2.0/3.0 hard disk drives
-	Linux NFS only (Windows-based machines are not supported in transfer jobs)
-	Service availability depends on the region – find supported regions list [here](https://docs.oracle.com/en-us/iaas/Content/DataTransfer/Concepts/supported_regions.htm)

Oracle provides two ways to manage disk-based data transfers:
- The Data Transfer Utility that is a full-featured command line tool for disk-based data transfers only. For more information go [here](https://docs.oracle.com/en-us/iaas/Content/DataTransfer/Tasks/disk_preparing.htm#InstallingDTU)
- The Console that is an easy-to-use, partial-featured browser-based interface. For more information go [here](https://docs.oracle.com/en-us/iaas/Content/GSG/Tasks/signingin.htm)

When it comes to the security aspects please find some highlights, however you can find more info [here](https://docs.oracle.com/en-us/iaas/Content/DataTransfer/Concepts/disk_overview.htm): 
-	Data encrypted (AES-256) before transit
- The Data Transfer Utility uses the standard Linux dm-crypt and LUKS utilities to encrypt block devices
-	Data uploaded to bucket, verified, then erased from disk
-	Network communication is encrypted using Transit Layer Security (TLS)


## How Data import works for Disk-based method?

As described above, this method lets you to send your data as files on an encrypted disk to an Oracle transfer site. Operators at the Oracle transfer site upload the files into the designated Object Storage bucket in your tenancy. You are then free to move the uploaded data to other Oracle Cloud Infrastructure services as needed. The whole data import process for this method is summarized below.
Please note that some steps require to use of the OCI CLI - not everything can be done via the OCI Console (find below which is required: CLI, Console, or can be both).

1. Procure USB 2.0/3.0 Disk
2. Create Transfer Job and specify disk - **OCI CLI** or **Console**
3. Establish IAM policies - **OCI CLI** or **Console**
4. Create Object Storage bucket - **OCI CLI** or **Console**
5. Install dts utility and CLI on Linux host
6. Create configuration files
7. Connect disk to Linux host
8. Prepare disk with dts utility
9. Verify disk mounted on Linux host
10. Upload RSA key pairs - **OCI CLI** or **Console**
11. Set HTTP proxy and firewall access
12. Copy data to disk mount point
13. ‘seal’ dataset to generate manifest
14. ‘finalize’ the appliance - **OCI CLI** 
15. Ship the disk to Oracle
16. Include the return label to have disk returned
17. Oracle uploads data to bucket
18. Monitor data transfer progress - **OCI CLI** or **Console**
19. Oracle wipes disk and returns to customer

Note: Individual files being copied to the disk cannot exceed 10 TB -

Detailed steps description can be found [here](https://docs.oracle.com/en-us/iaas/Content/DataTransfer/Tasks/disk_preparing.htm)


